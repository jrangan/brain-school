const mongoose = require("mongoose");

const MONGO_URI = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@mongo:27017/${process.env.MONGO_INITDB_DATABASE}`;
(async () => {
  await mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  userSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
  });

  const userModel = mongoose.model("User", userSchema);

  const userA = new userModel({
    firstName: "John",
    lastName: "Rambo",
  });

  await userA.save();

  const result = await userModel.find();

  console.log(result);

  await mongoose.connection.close();
})();

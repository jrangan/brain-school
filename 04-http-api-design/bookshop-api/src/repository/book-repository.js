class BookRepository {
  async getById(id) {}
  async save(book) {}
  async listAll() {}
  async cleanUp() {}
  async setup() {}
}

module.exports = BookRepository;

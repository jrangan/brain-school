const mongoose = require("mongoose");
const BookRepository = require("./book-repository");

class MongoBookRepository extends BookRepository {
  constructor() {
    super();
  }

  async setup() {
    await mongoose.connect("mongodb://localhost/bookshop", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    this.BookSchema = new mongoose.Schema({
      _id: String,
      title: String,
      author: String,
      price: Number,
    });
    this.Book = mongoose.model("Book", this.BookSchema);
  }

  convertFromMongo(book) {
    if (book) {
      return {
        id: book._id,
        title: book.title,
        author: book.author,
        price: book.price,
      };
    }
    return null;
  }

  async getById(id) {
    const book = await this.Book.findById(id);
    return this.convertFromMongo(book);
  }

  convertToMongo(book) {
    if (book) {
      return {
        _id: book.id,
        title: book.title,
        author: book.author,
        price: book.price,
      };
    }
    return null;
  }

  async save(mongoBook) {
    const book = new this.Book(this.convertToMongo(mongoBook));
    await book.save();
  }

  async listAll() {
    const books = await this.Book.find();
    return books.map(this.convertFromMongo);
  }

  async cleanUp() {
    await mongoose.connection.close();
  }
}

module.exports = MongoBookRepository;

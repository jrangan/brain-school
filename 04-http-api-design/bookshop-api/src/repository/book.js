const { v4: uuidv4 } = require("uuid");

class Book {
  constructor(title, author, price) {
    this.id = uuidv4();
    this.title = title;
    this.author = author;
    this.price = price;
  }
}

module.exports = Book;

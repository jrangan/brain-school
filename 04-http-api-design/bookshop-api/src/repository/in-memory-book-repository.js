const BookRepository = require("./book-repository");

class InMemoryBookRepository extends BookRepository {
  constructor() {
    super();
    this.books = new Map();
  }

  async getById(id) {
    return this.books.get(id);
  }
  async save(book) {
    this.books.set(book.id, book);
  }
  async listAll() {
    const iterator = this.books.values();
    return Array.from(iterator);
  }
}

module.exports = InMemoryBookRepository;

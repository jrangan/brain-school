# Visual Studio Code Dev Containers

This README file provides an overview of Visual Studio Code's (VS Code) Dev Containers, a feature that enhances development workflow and productivity by leveraging Docker and Docker Compose. This document explains the basics of Dev Containers and demonstrates how to use Docker Compose with Dev Containers, focusing on a Node.js environment with MongoDB. For a comprehensive understanding, refer to the official Microsoft documentation.

## Table of Contents

- [Visual Studio Code Dev Containers](#visual-studio-code-dev-containers)
  - [Table of Contents](#table-of-contents)
  - [Introduction to Dev Containers](#introduction-to-dev-containers)
  - [Docker Compose with Dev Containers](#docker-compose-with-dev-containers)
  - [Additional Resources](#additional-resources)

## Introduction to Dev Containers

Dev Containers are a VS Code feature that allows you to develop inside a Docker container. This approach provides a consistent and easily reproducible development environment, improving collaboration and minimizing "works on my machine" issues. With Dev Containers, you can:

- Configure and share consistent development environments
- Ensure development dependencies are isolated from your local machine
- Easily switch between different projects and environments

To get started with Dev Containers, you'll need the following prerequisites:

1. [Visual Studio Code](https://code.visualstudio.com/download)
2. [Docker Desktop](https://www.docker.com/products/docker-desktop) (for Windows and macOS) or Docker Engine (for Linux)
3. [Remote - Containers extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) for VS Code

Refer to Microsoft's [official documentation](https://code.visualstudio.com/docs/remote/containers) for a detailed explanation and a step-by-step guide.

## Docker Compose with Dev Containers

Docker Compose is a tool for defining and running multi-container Docker applications. Dev Containers support Docker Compose, allowing you to define multiple services and easily manage their dependencies.

To use Docker Compose with Dev Containers in a Node.js environment with MongoDB, follow these steps:

1. Create a `docker-compose.yml` file in your project's root directory. This file will define your services and their dependencies.

2. Define your services and their configurations in the `docker-compose.yml` file. For example:

   ```yaml
   version: "3.9"

   services:
     app:
       build:
         context: .
         dockerfile: Dockerfile
       command: sh -c "npm install && npm run watch"
       volumes:
         - ..:/workspace:cached
       environment:
         MONGO_URL: mongodb://db:27017/mydb

     db:
       image: mongo:4.4
       volumes:
         - db-data:/data/db

   volumes:
     db-data:
   ```

3. Create a `Dockerfile` in your project's root directory. This file will define the Node.js development environment. For example:

   ```dockerfile
   FROM node:14

   WORKDIR /workspace

   EXPOSE 3000
   ```

4. Create a `.devcontainer` directory in your project's root directory. Inside it, create a `devcontainer.json` file.

5. Configure the `devcontainer.json` file to use the Docker Compose file. For example:

   ```json
   {
     "dockerComposeFile": "../docker-compose.yml",
     "service": "app",
     "workspaceFolder": "/workspace",
     "settings": {
       "terminal.integrated.defaultProfile.linux": "/bin/bash"
     },
     "extensions": ["dbaeumer.vscode-eslint"]
   }
   ```

6. Open your project in VS Code, and click on the "Reopen in Container" prompt, or press F1 and select "Remote-Containers: Reopen Folder in Container" from the command palette.

7. Your development environment will be created inside the specified Docker container, and you can start developing.

For more details on using Docker Compose with Dev Containers, refer to the official documentation.

## Additional Resources

- [VS Code Dev Containers Documentation](https://code.visualstudio.com/docs/remote/containers)
- [VS Code Remote Development Documentation](https://code.visualstudio.com/docs/remote/remote-overview)
- [Docker Compose Documentation](https://docs.docker.com/compose/)
- [Docker Documentation](https://docs.docker.com/)

By using VS Code's Dev Containers, you can streamline your development workflow and improve the overall development experience. Remember to refer to the official Microsoft documentation for in-depth information and guidance. Enjoy coding with Dev Containers and Docker Compose in a Node.js environment with MongoDB!

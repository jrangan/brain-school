const halson = require("halson");

describe("HATEOAS", () => {
  describe("Create a HAL Document", () => {
    test("should return a HAL document with a self link", () => {
      const hal = halson({ name: "John Doe" });
      hal.addLink("self", "/users/1");

      expect(hal).toEqual({
        _links: {
          self: {
            href: "/users/1",
          },
        },
        name: "John Doe",
      });
    });

    test("should return a HAL document with a link to related resource", () => {
      const hal = halson({ name: "John Doe" });
      const related = hal.addLink("related", "/posts");

      expect(hal).toEqual({
        _links: {
          related: {
            href: "/posts",
          },
        },
        name: "John Doe",
      });
    });
  });
  describe("Consume a HAL Document", () => {
    let hal;
    beforeEach(() => {
      hal = halson({
        _links: {
          self: {
            href: "/users/1",
          },
          related: {
            href: "/posts",
          },
        },
        name: "John Doe",
      });
    });

    test("Should return a link to self from an existing hal document resource", () => {
      const selfLink = hal.getLink("self");
      expect(selfLink).toEqual({ href: "/users/1" });
    });
    test("Should return a link related to an existing hal document resource", () => {
      const relatedLink = hal.getLinks("related");
      expect(relatedLink).toEqual([{ href: "/posts" }]);
    });
  });
});

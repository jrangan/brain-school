const halson = require("halson");

describe("HATEOAS", () => {
  describe("creating HAL documents", () => {
    test("should return a HAL document with a self link", () => {
      const hal = halson({ name: "John Doe" });
      hal.addLink("self", "/users/1");

      expect(hal).toEqual({
        _links: {
          self: {
            href: "/users/1",
          },
        },
        name: "John Doe",
      });
    });

    test("should return a HAL document with a link to related resource", () => {
      const hal = halson({ name: "John Doe" });
      const related = { href: "/posts" };
      hal.addLink("related", related);

      expect(hal).toEqual({
        _links: {
          related: {
            href: "/posts",
          },
        },
        name: "John Doe",
      });
    });

    test("should return a HAL document with an embedded resource", () => {
      const hal = halson({ name: "John Doe" });
      const embed = {
        _links: {
          self: { href: "/profile" },
        },
        aboutMe: "Some guy called John",
      };
      hal.addEmbed("profile", embed);

      expect(hal).toEqual({
        _embedded: {
          profile: {
            _links: {
              self: { href: "/profile" },
            },
            aboutMe: "Some guy called John",
          },
        },
        name: "John Doe",
      });
    });
  });

  describe("querying HAL documents", () => {
    let hal;

    beforeEach(() => {
      hal = halson({
        _links: {
          self: {
            href: "/users/1",
          },
          related: [{ href: "/posts" }, { href: "/profile" }],
        },
        _embedded: {
          profile: {
            _links: {
              self: { href: "/profile" },
            },
            aboutMe: "Some guy called John",
          },
        },
        name: "John Doe",
      });
    });

    test("should get a list of link relations", () => {
      const relations = hal.listLinkRels();
      expect(relations).toEqual(["self", "related"]);
    });

    test("should get the first related link", () => {
      const related = hal.getLink("related");

      expect(related).toEqual({
        href: "/posts",
      });
    });

    test("should get all the related links", () => {
      const related = hal.getLinks("related");

      expect(related).toEqual([{ href: "/posts" }, { href: "/profile" }]);
    });

    test("should get a list of embedded relations", () => {
      const relations = hal.listEmbedRels();
      expect(relations).toEqual(["profile"]);
    });

    test("should get an embedded resource", () => {
      const embed = hal.getEmbed("profile");
      expect(embed).toEqual({
        _links: {
          self: { href: "/profile" },
        },
        aboutMe: "Some guy called John",
      });
    });
  });
});

const request = require("supertest");
const app = require("../app.js");
const { expect, assert } = require("chai");

describe("GET /", () => {
  let server;
  beforeEach(async () => {
    server = new app();
    await server.start();
  });

  afterEach(async () => {
    await server.stop();
  });

  it('should respond with "Hello, World!"', async () => {
    const res = await request(server.app).get("/");
    expect(res.status).to.equal(200);
    expect(res.text).to.equal("Hello, World!");
  });
  it("should create a new resource", async () => {
    const res = await request(server.app)
      .post("/resource")
      .send({ name: "New resource" });
    expect(res.status).to.equal(201);
    expect(res.body).to.have.property("id");
    expect(res.body.name).to.equal("New resource");
  });
  it("should return a 400 error for invalid input", async () => {
    const res = await request(server.app)
      .post("/resource")
      .send({ wrongKey: "Wrong value" });
    expect(res.status).to.equal(400);
    expect(res.body).to.have.property("error");
  });
  it("should handle a get request with parameters", async () => {
    const res = await request(server.app)
      .get("/search")
      .query({ name: "test" });
    expect(res.status).to.equal(200);
    expect(res.body).to.deep.equal([{ id: "1", name: "test" }]);
  });
  it("should handle delete request", async () => {
    const res = await request(server.app).delete("/resource/55");
    expect(res.status).to.equal(204);
  });
  it("should handle put request", async () => {
    const res = await request(server.app)
      .put("/resource/55")
      .send({ name: "person" });
    expect(res.status).to.equal(200);
    expect(res.body).to.deep.equal({ id: "55", name: "person" });
  });
});

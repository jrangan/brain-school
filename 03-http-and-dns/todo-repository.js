class TodoRepository {
  async getAll() {}

  async getById(id) {}

  async save(todo) {}

  async remove(id) {}

  async updateName(id, newTodoName) {}

  async updateStatus(id, newStatus) {}

  async getTodoIndexById(id) {}

  async getTodoIndex(todoName) {}

  async hasTodo(id) {}

  async setup() {}

  async cleanUp() {}
}

module.exports = TodoRepository

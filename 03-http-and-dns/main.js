const TodoServer = require('./server.js')
const MongoRepository = require('./mongo-todo-repository.js')

const mongoTodoRepo = new MongoRepository()
const todoServer = new TodoServer(mongoTodoRepo)
todoServer.start()

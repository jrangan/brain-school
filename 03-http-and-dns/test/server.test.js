const request = require('supertest')
const TodoServer = require('../server.js')
const MongoRepository = require('../mongo-todo-repository.js')

describe('server', () => {
  let todoServer
  let mongoTodoRepo
  beforeEach(async () => {
    mongoTodoRepo = new MongoRepository()
    todoServer = new TodoServer(mongoTodoRepo)
    await todoServer.start()
  })
  afterEach(async () => await todoServer.stop())

  describe('HTTP Requests', () => {
    describe('GET Requests', () => {
      it('Empty todo list', async () => {
        const response = await request(todoServer.server).get('/todo')
        expect(response.status).toBe(200)
        expect(response.body).toEqual({
          _links: { create: { href: '/todo', method: 'POST' } },
          todoList: [],
        })
      })
      it('A todo list with one todo', async () => {
        const todo = { id: '1', name: 'Task1', complete: true }
        await mongoTodoRepo.save(todo)
        const response = await request(todoServer.server).get('/todo')
        expect(response.status).toBe(200)
        expect(response.body).toEqual({
          _links: { create: { href: '/todo', method: 'POST' } },
          todoList: [
            {
              _links: { self: { href: '/todo/1' } },
              complete: true,
              id: '1',
              name: 'Task1',
            },
          ],
        })
      })
      it('A todo list with two todos', async () => {
        await mongoTodoRepo.save({ id: '1', name: 'Task1', complete: true })
        await mongoTodoRepo.save({ id: '2', name: 'Task2', complete: true })
        const response = await request(todoServer.server).get('/todo')
        expect(response.status).toBe(200)
        expect(response.body).toEqual({
          _links: { create: { href: '/todo', method: 'POST' } },
          todoList: [
            {
              _links: { self: { href: '/todo/1' } },
              id: '1',
              name: 'Task1',
              complete: true,
            },
            {
              _links: { self: { href: '/todo/2' } },
              id: '2',
              name: 'Task2',
              complete: true,
            },
          ],
        })
      })
      it('Get a specific todo', async () => {
        await mongoTodoRepo.save({ id: '1', name: 'Task1', complete: true })
        await mongoTodoRepo.save({ id: '2', name: 'Task2', complete: true })
        const response = await request(todoServer.server).get('/todo/2')
        expect(response.body).toEqual({
          _links: {
            self: { href: '/todo/2' },
            update: { href: '/todo/2', method: 'PATCH' },
            delete: { href: '/todo/2', method: 'DELETE' },
          },
          id: '2',
          name: 'Task2',
          complete: true,
        })
      })
      it('Get a specific todo not in the list', async () => {
        await mongoTodoRepo.save({ id: '1', name: 'Task1', complete: true })
        await mongoTodoRepo.save({ id: '2', name: 'Task2', complete: true })
        const response = await request(todoServer.server).get('/todo/999')
        expect(response.body).toEqual({})
      })
    })
    describe('POST Requests', () => {
      it('Create one todo', () => {
        let todo = { id: '1', name: 'Task1', complete: false }
        const response = request(todoServer.server).post('/todo').send(todo)
        expect(response._data).toEqual(todo)
      })
      it('Create two todo', async () => {
        let todo1 = { id: expect.any(String), name: 'Task1', complete: false }
        let todo2 = { id: expect.any(String), name: 'Task2', complete: false }
        await request(todoServer.server).post('/todo').send(todo1)
        await request(todoServer.server).post('/todo').send(todo2)
        const list = await mongoTodoRepo.getAll()
        expect(list).toEqual([todo1, todo2])
      })
    })
    describe('PATCH', () => {
      it('Updates a todo name', async () => {
        let todo = { name: 'Task1', complete: false, id: '1' }
        await mongoTodoRepo.save(todo)
        const response = await request(todoServer.server)
          .patch('/todo/1')
          .send({ name: 'NewTask' })
        expect(response.body).toEqual({
          id: '1',
          name: 'NewTask',
          complete: false,
        })
      })
      it('Updates a todo status', async () => {
        let todo = { name: 'Task2', complete: false, id: '1' }
        await mongoTodoRepo.save(todo)
        const response = await request(todoServer.server)
          .patch('/todo/1')
          .send({ complete: true })
        expect(response.body).toEqual({
          id: '1',
          name: 'Task2',
          complete: true,
        })
      })
      it('Updates a todo with invalid request body', async () => {
        let todo = { name: 'Task2', complete: false, id: '1' }
        await mongoTodoRepo.save(todo)
        const response = await request(todoServer.server)
          .patch('/todo/1')
          .send({ key: 'invalid' })
        expect(response.status).toEqual(400)
      })
    })
    describe('DELETE Requests', () => {
      it('Delete a todo from a list', async () => {
        let todo = { name: 'Task2', complete: false, id: '1' }
        await mongoTodoRepo.save(todo)
        const response = await request(todoServer.server).delete('/todo/1')
        expect(response.status).toEqual(200)
      })
      it('Delete second todo from a list of 3', async () => {
        await mongoTodoRepo.save({ name: 'Task1', complete: false, id: '1' })
        await mongoTodoRepo.save({ name: 'Task2', complete: false, id: '2' })
        await mongoTodoRepo.save({ name: 'Task3', complete: false, id: '3' })
        const response = await request(todoServer.server).delete('/todo/2')
        expect(response.status).toEqual(200)
        const data = await mongoTodoRepo.getAll()
        expect(data).toEqual([
          { name: 'Task1', complete: false, id: '1' },
          { name: 'Task3', complete: false, id: '3' },
        ])
      })
      it('Delete a todo not in the list', async () => {
        let todo = { name: 'Task2', complete: false, id: '1' }
        await mongoTodoRepo.save(todo)
        const response = await request(todoServer.server).delete('/todo/999')
        expect(response.status).toEqual(400)
      })
    })
  })
})

const { mongo } = require('mongoose')
const mongoRepo = require('../mongo-todo-repository')
const todo = require('../todo')

describe('database-mongo', () => {
  let mongo
  beforeEach(async () => {
    mongo = new mongoRepo()
    await mongo.setup()
  })
  afterEach(async () => {
    await mongo.cleanUp()
  })
  describe('reading', () => {
    it('get empty todos', async () => {
      const data = await mongo.getAll()
      expect(data).toEqual([])
    })
    it('get the first todo by the id in a list', async () => {
      const todo = { id: '1', name: 'Task1', complete: false }
      await mongo.save(todo)
      const data = await mongo.getById('1')
      expect(data).toEqual({ id: '1', name: 'Task1', complete: false })
    })
    describe('getting a todo by id', () => {
      it('get the second todo by the id in a list', async () => {
        await mongo.save({ id: '1', name: 'Task1', complete: false })
        await mongo.save({ id: '2', name: 'Task2', complete: false })
        await mongo.save({ id: '3', name: 'Task3', complete: false })
        const data = await mongo.getById('2')
        expect(data).toEqual({ id: '2', name: 'Task2', complete: false })
      })
      it('returns true if valid id is passed', async () => {
        await mongo.save({ id: '1', name: 'Task1', complete: false })
        await mongo.save({ id: '2', name: 'Task2', complete: false })
        await mongo.save({ id: '3', name: 'Task3', complete: false })
        const data = await mongo.hasTodo('2')
        expect(data).toEqual(true)
      })
      it('returns false if invalid id is passed', async () => {
        await mongo.save({ id: '1', name: 'Task1', complete: false })
        await mongo.save({ id: '2', name: 'Task2', complete: false })
        const data = await mongo.hasTodo('3')
        expect(data).toEqual(false)
      })
    })
  })
  describe('adding', () => {
    it('add todos', async () => {
      const todo = { id: '1', name: 'Task1', complete: false }
      await mongo.save(todo)
      const data = await mongo.getAll()
      expect(data).toEqual([{ complete: false, id: '1', name: 'Task1' }])
    })
  })
  describe('updating', () => {
    it('update the name of a todo in a list', async () => {
      await mongo.save({ id: '1', name: 'Task1', complete: false })
      await mongo.updateName('1', 'NewTask')
      const data = await mongo.getAll()
      expect(data).toEqual([{ id: '1', name: 'NewTask', complete: false }])
    })
    it('update the status of a todo in a list', async () => {
      await mongo.save({ id: '1', name: 'Task1', complete: false })
      await mongo.updateStatus('1', true)
      const data = await mongo.getAll()
      expect(data).toEqual([{ id: '1', name: 'Task1', complete: true }])
    })
  })
  describe('deleting', () => {
    it('delete one todo from a list of todos', async () => {
      await mongo.save({ id: '1', name: 'Task1', complete: false })
      await mongo.remove('1')
      const data = await mongo.getAll()
      expect(data).toEqual([])
    })
    it('delete a specific todo from a list of todos', async () => {
      await mongo.save({ id: '1', name: 'Task1', complete: false })
      await mongo.save({ id: '2', name: 'Task2', complete: false })
      await mongo.save({ id: '3', name: 'Task3', complete: false })
      await mongo.remove('2')
      const data = await mongo.getAll()
      expect(data).toEqual([
        { id: '1', name: 'Task1', complete: false },
        { id: '3', name: 'Task3', complete: false },
      ])
    })
  })
})

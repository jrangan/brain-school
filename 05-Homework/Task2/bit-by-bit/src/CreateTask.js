import * as React from "react";
import PropTypes from "prop-types";

//ADDS TASK TO TASK LIST

const CreateTask = ({ updateTasks }) => {
  const [taskName, setTaskName] = React.useState(""); //Task Name (State)
  //On change of input textboxes, store string value in the state
  const handleOnChangeNameEvent = ({ target: { value } }) => setTaskName(value);

  const handleOnClickEvent = () => {
    //On click pass the name and desc values to the function that adds items to the todoList array
    updateTasks(taskName);

    //Clear input elements
    setTaskName("");
  };
  return (
    <div>
      <h2>CREATE TODO</h2>
      <h3>PLEASE ENTER A TODO</h3>
      <input
        name="taskName"
        value={taskName}
        onChange={handleOnChangeNameEvent}
      />
      <button onClick={handleOnClickEvent}>ADD TODO</button>
    </div>
  );
};

CreateTask.propTypes = {
  updateTasks: PropTypes.func,
};

CreateTask.defaultProps = {
  updateTasks: () => {},
};
export default CreateTask;

const apiGetAll = async (url, updateData) => {
  return await fetch(url)
    .then((response) => response.json())
    .then((data) => updateData(data.todoList))
    .catch((error) => console.log(error));
};

const apiPost = async (url, todoId, todoName, todoComplete) => {
  return await fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      id: todoId,
      name: todoName,
      complete: todoComplete,
    }),
  });
};

const apiPatch = async (url, id, newValue, todoProperty) => {
  return await fetch(url + "/" + id, {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ [todoProperty]: newValue }),
  });
};

const apiDelete = async (url, id) => {
  return await fetch(url + "/" + id, {
    method: "DELETE",
  });
};

export { apiGetAll, apiPost, apiPatch, apiDelete };

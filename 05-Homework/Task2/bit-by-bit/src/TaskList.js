import * as React from "react";
import PropTypes from "prop-types";

//DISPLAYS LIST OF TASKS

const TaskList = (props) => {
  const {
    updatedName,
    updatedTaskName,
    removeTask,
    setComplete,
    tasks = [],
  } = props; //todoList [] passed in as props

  const handleUpdateTask = (id, updatedName, updatedTaskName) => {
    updatedTaskName(id, updatedName);
  };

  const handleRemoveTask = (id, removeTask) => {
    removeTask(id);
  };

  const handleStrike = (id, setComplete) => {
    setComplete(id);
  };

  return (
    <React.Fragment>
      <h1>TODO LIST</h1>
      <ul>
        {tasks.map((task) => {
          return (
            <React.Fragment>
              {task.done === false ? (
                <li
                  key={task.id}
                  onClick={() => handleStrike(task.id, setComplete)}
                >
                  {task.name}
                </li>
              ) : (
                <li
                  key={task.id}
                  onClick={() => handleStrike(task.id, setComplete)}
                  style={{ textDecoration: "line-through" }}
                >
                  {task.name}
                </li>
              )}
              <button
                type="button"
                onClick={() =>
                  handleUpdateTask(task.id, updatedName, updatedTaskName)
                }
              >
                UPDATE
              </button>
              <button
                type="button"
                onClick={() => handleRemoveTask(task.id, removeTask)}
              >
                REMOVE
              </button>
            </React.Fragment>
          );
        })}
      </ul>
    </React.Fragment>
  );
};

TaskList.propTypes = {
  updatedName: PropTypes.string,
  updatedTaskName: PropTypes.func,
  removeTask: PropTypes.func,
  setComplete: PropTypes.func,
  tasks: PropTypes.array,
};

TaskList.defaultProps = {
  updatedName: "",
  updatedTaskName: () => {},
  removeTask: () => {},
  setComplete: () => {},
  tasks: [],
};

export default TaskList;

const TodoRepository = require('../todo-repository')
class InMemoryTodoRepository extends TodoRepository {
  constructor() {
    super()
    this.todoList = []
  }

  async getAll() {
    return this.todoList
  }

  async getById(id) {
    return this.todoList[this.getTodoIndexById(id)]
  }

  async save(todo) {
    this.todoList.push(todo)
  }

  async remove(id) {
    const indexOfItem = this.getTodoIndexById(id)
    this.todoList.splice(indexOfItem, 1)
  }

  async updateName(id, newTodoName) {
    const indexOfItem = this.getTodoIndexById(id)
    this.todoList[indexOfItem].name = newTodoName
  }

  async updateStatus(id, newStatus) {
    const indexOfItem = this.getTodoIndexById(id)
    this.todoList[indexOfItem].complete = newStatus
  }

  async hasTodo(id) {
    return this.getTodoIndexById(id) != -1
  }

  getTodoIndexById(id) {
    return this.todoList.findIndex((item) => item.id === id)
  }

  getTodoIndex(todoName) {
    return this.todoList.findIndex((item) => item.name === todoName)
  }
}

module.exports = InMemoryTodoRepository

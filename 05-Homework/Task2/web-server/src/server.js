const TodoApi = require('./todo-api.js')
const express = require('express')
const bodyParser = require('body-parser')
const halson = require('halson')

class TodoServer {
  // Create a new server instance
  constructor(repository) {
    this.app = express()
    this.app.use(bodyParser.json())
    this.todoApi = new TodoApi(repository)
    this.repository = repository

    this.app.get('/todo', async (request, response) => {
      const todoResponse = await this.todoApi.retrieveTodos()
      const todoList = todoResponse.data
      const halsonArray = []
      todoList.forEach((todo) => {
        const hal = halson(todo)
        hal.addLink('self', `/todo/${todo.id}`)
        halsonArray.push(hal)
      })
      const halList = halson({ todoList: halsonArray })
      halList.addLink('create', {
        href: `/todo`,
        method: 'POST',
      })
      const responseStatus = this.checkStatus(todoResponse.status)
      response.status(responseStatus).json(halList)
    })
    this.app.get('/todo/:id', async (request, response) => {
      const todoResponse = await this.todoApi.retrieveTodoById(
        request.params.id,
      )
      const hal = halson(todoResponse.data)
      hal.addLink('self', `/todo/${todoResponse.data.id}`)
      hal.addLink('update', {
        href: `/todo/${todoResponse.data.id}`,
        method: 'PATCH',
      })
      hal.addLink('delete', {
        href: `/todo/${todoResponse.data.id}`,
        method: 'DELETE',
      })
      const responseStatus = this.checkStatus(todoResponse.status)
      if (responseStatus === 200) {
        response.status(responseStatus).json(hal)
      }
      if (responseStatus === 400) {
        response.status(responseStatus).json(todoResponse.data)
      }
    })
    this.app.post('/todo', async (request, response) => {
      const todoResponse = await this.todoApi.createTodo(request.body)
      const responseStatus = this.checkStatus(todoResponse.status)
      response.status(responseStatus).json(todoResponse.data)
    })
    this.app.patch('/todo/:id', async (request, response) => {
      let todoResponse
      if ('name' in request.body) {
        todoResponse = await this.handleUpdateName(
          request.params.id,
          request.body,
        )
      } else if ('complete' in request.body) {
        todoResponse = await this.handleUpdateStatus(
          request.params.id,
          request.body,
        )
      } else {
        todoResponse = { status: 'fail', data: {} }
      }
      const responseStatus = this.checkStatus(todoResponse.status)
      response.status(responseStatus).json(todoResponse.data)
    })
    this.app.delete('/todo/:id', async (request, response) => {
      const todoResponse = await this.todoApi.removeTodo(request.params.id)
      const responseStatus = this.checkStatus(todoResponse.status)
      response.status(responseStatus).json(todoResponse.data)
    })
  }

  async handleUpdateName(id, todo) {
    return await this.todoApi.updateItemName(id, todo)
  }
  async handleUpdateStatus(id, todo) {
    return await this.todoApi.updateItemStatus(id, todo)
  }

  checkStatus(status) {
    if (status === 'success') {
      return 200
    }
    if (status === 'fail') {
      return 400
    }
  }

  async start() {
    await this.repository.setup()
    return new Promise((resolve, reject) => {
      try {
        this.server = this.app.listen(5000, async () => {
          resolve()
          console.log('Server Started on Port 5000')
        })
      } catch (err) {
        reject(err)
      }
    })
  }

  async stop() {
    await this.repository.cleanUp()
    return new Promise((resolve, reject) => {
      try {
        this.server.close(async () => resolve())
      } catch (err) {
        reject(err)
      }
    })
  }
}

if (require.main == module) {
  const server = new TodoServer()
  server.start()
}

module.exports = TodoServer

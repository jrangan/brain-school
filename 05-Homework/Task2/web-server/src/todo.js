const { v4: uuidv4 } = require('uuid')

class Todo {
  constructor(name) {
    this.name = name
    this.complete = false
    this.id = uuidv4()
  }
}

module.exports = Todo

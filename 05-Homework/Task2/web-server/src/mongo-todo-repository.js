const mongoose = require('mongoose')
const TodoRepository = require('./todo-repository')

class MongoTodoRepository extends TodoRepository {
  constructor() {
    super()
  }
  async setup() {
    await mongoose.connect('mongodb://localhost/todo', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    this.TodoSchema = new mongoose.Schema({
      _id: String,
      name: String,
      complete: Boolean,
    })
    this.Todo = mongoose.model('Todo', this.TodoSchema)
  }

  async cleanUp() {
    //For testing - delete after
    await this.Todo.deleteMany({})
    mongoose.deleteModel(/.+/)

    //Production only
    await mongoose.connection.close()
  }

  convertFromMongo(todo) {
    if (todo) {
      return {
        id: todo._id,
        name: todo.name,
        complete: todo.complete,
      }
    }
    return null
  }
  convertToMongo(todo) {
    if (todo) {
      return {
        _id: todo.id,
        name: todo.name,
        complete: todo.complete,
      }
    }
    return null
  }

  async getAll() {
    const list = await this.Todo.find()
    const newList = list.map(this.convertFromMongo)
    return newList
  }

  async getById(id) {
    const listItem = await this.Todo.findById(id)
    return this.convertFromMongo(listItem)
  }

  async save(todo) {
    const list = new this.Todo(this.convertToMongo(todo))
    const save = await list.save()
  }

  async remove(id) {
    await this.Todo.findByIdAndDelete(id)
  }

  async updateName(id, newTodoName) {
    await this.Todo.findByIdAndUpdate(id, {
      name: newTodoName,
    })
  }

  async updateStatus(id, newStatus) {
    await this.Todo.findByIdAndUpdate(id, {
      complete: newStatus,
    })
  }
  async hasTodo(id) {
    const todoItem = await this.Todo.findById(id)
    if (todoItem !== null) {
      return true
    } else if (todoItem === null) {
      return false
    } else {
      return null
    }
  }
  /*    getTodoIndexById(id) {
      return this.todoList.findIndex((item) => item.id === id)
    }
  
    getTodoIndex(todoName) {
      return this.todoList.findIndex((item) => item.name === todoName)
    }
  */
}

module.exports = MongoTodoRepository

const TodoApi = require('../src/todo-api')
const Todo = require('../src/todo')
const InMemoryTodoReposity = require('../src/in-memory-todo-repository.js')

describe('todo-api', () => {
  let todoApi
  let repository

  beforeEach(() => {
    repository = new InMemoryTodoReposity()
    todoApi = new TodoApi(repository)
  })
  describe('getting', () => {
    it('Get an empty list of todos', async () => {
      expect(await todoApi.retrieveTodos()).toEqual({
        status: 'success',
        data: [],
      })
    })
    it('Retrieve todo list', async () => {
      const todo = new Todo('Task1')
      await repository.save(todo)
      expect(await todoApi.retrieveTodos()).toEqual({
        status: 'success',
        data: [todo],
      })
    })
    it('Retrieve a specific todo', async () => {
      await repository.save({ id: '1', name: 'Task1', complete: false })
      await repository.save({ id: '2', name: 'Task2', complete: false })

      expect(await todoApi.retrieveTodoById('2')).toEqual({
        status: 'success',
        data: { id: '2', name: 'Task2', complete: false },
      })
    })
  })
  describe('creating', () => {
    it('Create todo list item', async () => {
      let response = await todoApi.createTodo({
        name: 'Task1',
        complete: false,
      })
      expect(response.status).toEqual('success')
      expect(response.data.name).toEqual('Task1')
      expect(response.data.complete).toEqual(false)
    })
  })
  describe('updating', () => {
    it('Update todo item name', async () => {
      const todo = new Todo('Task1')
      await repository.save(todo)
      const newTodoName = { name: 'NewTask' }
      expect(await todoApi.updateItemName(todo.id, newTodoName)).toEqual({
        status: 'success',
        data: { id: todo.id, name: 'NewTask', complete: false },
      })
    })
    it('Update todo item status to true from false', async () => {
      const todo = new Todo('Task1')
      await repository.save(todo)
      expect(
        await todoApi.updateItemStatus(todo.id, { complete: true }),
      ).toEqual({
        status: 'success',
        data: { id: todo.id, name: todo.name, complete: true },
      })
    })
    it('Update todo item status to false from true', async () => {
      const todo = new Todo('Task1')
      todo.complete = true
      await repository.save(todo)
      expect(
        await todoApi.updateItemStatus(todo.id, { complete: false }),
      ).toEqual({
        status: 'success',
        data: { id: todo.id, name: todo.name, complete: false },
      })
    })
    it('returns a failing status when updating the name of a non existent todo', async () => {
      expect(await todoApi.updateItemName('Task1', 'NewTaskName')).toEqual({
        status: 'fail',
        data: {},
      })
    })
    it('returns a failing status when updating the status of a non existent todo', async () => {
      expect(await todoApi.updateItemStatus('Task1')).toEqual({
        status: 'fail',
        data: {},
      })
    })
  })
  describe('removing', () => {
    it('Remove todo item', async () => {
      const todo = new Todo('Task1')
      await repository.save(todo)
      expect(await todoApi.removeTodo(todo.id)).toEqual({
        status: 'success',
        data: {},
      })
    })
    it('returns a failing status when removing a non existent todo', async () => {
      expect(await todoApi.removeTodo('Task1')).toEqual({
        status: 'fail',
        data: {},
      })
    })
  })
})

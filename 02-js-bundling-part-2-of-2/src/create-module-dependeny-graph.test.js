const createModuleDependencyGraph = require("./create-module-dependency-graph.js");
const path = require("path");
describe("create-module-dependency-graph", () => {
  describe("given a single module: A", () => {
    it("returns a graph with a single node", () => {
      const relativePathToModuleB =
        "./test-modules/folder-a/folder-b/module-b.js";
      const absolutePathToModuleB = path.join(
        __dirname,
        "./test-modules/folder-a/folder-b/module-b.js"
      );
      expect(
        createModuleDependencyGraph(__dirname, relativePathToModuleB)
      ).toEqual(
        expect.objectContaining({
          filePath: absolutePathToModuleB,
          dependencies: [],
        })
      );
    });
  });
  describe("given 2 modules: B<--depends-on--A", () => {
    it("returns a graph with two nodes", () => {
      const relativePathToModuleA = "./test-modules/folder-a/module-a.js";
      const absolutePathToModuleA = path.join(
        __dirname,
        "./test-modules/folder-a/module-a.js"
      );
      const absolutePathToModuleB = path.join(
        __dirname,
        "./test-modules/folder-a",
        "./folder-b/module-b.js"
      );
      expect(
        createModuleDependencyGraph(__dirname, relativePathToModuleA)
      ).toEqual(
        expect.objectContaining({
          filePath: absolutePathToModuleA,
          dependencies: expect.arrayContaining([
            expect.objectContaining({
              filePath: absolutePathToModuleB,
              dependencies: [],
            }),
          ]),
        })
      );
    });
  });

  describe("given 3 modules: C<--depends-on--B<--depends-on--A", () => {
    it("returns a graph with three nodes", () => {
      const relativePathToModuleC = "./test-modules/module-c.js";
      const absolutePathToModuleC = path.join(
        __dirname,
        "./test-modules/module-c.js"
      );
      const absolutePathToModuleA = path.join(
        __dirname,
        "./test-modules/",
        "./folder-a/module-a.js"
      );
      const absolutePathToModuleB = path.join(
        __dirname,
        "./test-modules/folder-a",
        "./folder-b/module-b.js"
      );
      expect(
        createModuleDependencyGraph(__dirname, relativePathToModuleC)
      ).toEqual(
        expect.objectContaining({
          filePath: absolutePathToModuleC,
          dependencies: expect.arrayContaining([
            expect.objectContaining({
              filePath: absolutePathToModuleA,
              dependencies: expect.arrayContaining([
                expect.objectContaining({
                  filePath: absolutePathToModuleB,
                  dependencies: [],
                }),
              ]),
            }),
          ]),
        })
      );
    });
  });
  describe("given 2 modules: B<--depends-on-->A", () => {
    it("returns a graph with 3 nodes", () => {
      const relativePathToModuleX =
        "./test-modules/cyclical-dependency/module-x.js";
      const absolutePathToModuleX = path.join(__dirname, relativePathToModuleX);
      const absolutePathToModuleY = path.join(
        __dirname,
        "./test-modules/cyclical-dependency/module-y.js"
      );
      expect(
        createModuleDependencyGraph(__dirname, relativePathToModuleX)
      ).toEqual(
        expect.objectContaining({
          filePath: absolutePathToModuleX,
          dependencies: [
            expect.objectContaining({
              filePath: absolutePathToModuleY,
              dependencies: [],
            }),
          ],
        })
      );
    });
  });
});

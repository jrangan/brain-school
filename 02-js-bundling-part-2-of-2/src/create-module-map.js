const collectModules = (module, modules = []) => {
  modules.push(module);
  module.dependencies.forEach((dependency) => {
    collectModules(dependency, modules);
  });
  return modules;
};

const createModuleMap = (moduleDepedencyGraph) => {
  const modules = collectModules(moduleDepedencyGraph);
  const moduleEntries = modules
    .map(
      (module) =>
        `'${module.filePath}': function (exports,require){ ${module.content} }`
    )
    .join(",");
  return `{ ${moduleEntries} }`;
};
module.exports = createModuleMap;

const fs = require("fs");
const path = require("path");
const bundle = require("./bundle");

describe("bundle", () => {
  describe("given an entry point and an output folder", () => {
    const outputDirPath = path.join(__dirname, ".tmp");
    beforeAll(() => {
      if (!fs.existsSync(outputDirPath)) {
        fs.mkdirSync(outputDirPath);
      }
    });

    it("writes a bundle into the output folder", () => {
      bundle({
        entryFile: "./test-modules/module-c.js",
        entryFilePath: __dirname,
        outputFolder: outputDirPath,
      });
      const bundledCode = fs.readFileSync(
        path.join(outputDirPath, "bundle.js"),
        {
          encoding: "utf-8",
        }
      );
      expect(bundledCode).toMatchInlineSnapshot(`
        "const modules = { '/home/josh/Desktop/workshops/bs-workshops/brain-school/02-js-bundling-part-2-of-2/src/test-modules/module-c.js': function (exports,require){ const _temp = require("/home/josh/Desktop/workshops/bs-workshops/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/module-a.js");
        console.log(\`So yeah, \${_temp["default"]}\`); },'/home/josh/Desktop/workshops/bs-workshops/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/module-a.js': function (exports,require){ const _temp = require("/home/josh/Desktop/workshops/bs-workshops/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/folder-b/module-b.js");
        const a = "hey " + _temp["default"];
        exports.default = a; },'/home/josh/Desktop/workshops/bs-workshops/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/folder-b/module-b.js': function (exports,require){ const b = "bebe";
        exports.default = b; } };
        const entry = "/home/josh/Desktop/workshops/bs-workshops/brain-school/02-js-bundling-part-2-of-2/src/test-modules/module-c.js";
        function webpackStart({modules,entry}){
        const moduleCache = {};
        //Webpack require function
        const require = moduleName => {
        //If the module is cached, return the cached version
        if(moduleCache[moduleName]){
        return moduleCache[moduleName];
        }
        const exports = {};
        //We need to avoid the cyclical dependencies
        //when invoking require()
        moduleCache[moduleName] = exports;
        //require() the module 
        modules[moduleName](exports,require);
        return moduleCache[moduleName];
        };
        //Execute the program
        require(entry);
        }
        webpackStart({modules,entry});
        "
      `);
    });
  });
});
